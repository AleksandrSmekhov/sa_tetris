import { PLAYFIELD_COLUMNS } from "./const.js"

export const countScore = (number) => {
    let result = 0

    for (let i = 0; i < number; i++) {
        result += PLAYFIELD_COLUMNS * (i + 1)
    }

    return result
}
