export const rotateMatrix = (matrix) => {
    const N = matrix.length
    const rotatedMatrix = []

    for (let i = 0; i < N; i++) {
        rotatedMatrix[i] = []
        for (let j = 0; j < N; j++) {
            rotatedMatrix[i][j] = matrix[N - j - 1][i]
        }
    }

    return rotatedMatrix
}
