import { PLAYFIELD_COLUMNS, NEXT_FIELD_COLUMS } from "./const.js"

export const convertPostionToIndex = (row, column) => {
    return row * PLAYFIELD_COLUMNS + column
}

export const convertPositionToIndexOfNext = (row, column) => {
    return row * NEXT_FIELD_COLUMS + column
}
