import { Tetris } from "./tetris.js"

import {
    convertPostionToIndex,
    convertPositionToIndexOfNext,
} from "./utils/convertPostionToIndex.js"

import {
    PLAYFIELD_ROWS,
    PLAYFIELD_COLUMNS,
    MOVE_THRESHOLD,
    SWIPE_THRESHOLD,
    SAD,
    SAD_TOP_OFFSET,
} from "./utils/const.js"

export class Board {
    constructor() {
        this.requestId
        this.timeoutId
        this.touchTimeout
        this.shortTouch = true
        this.tetris = new Tetris()
        this.cells = document.querySelectorAll(".grid>div")
        this.nextCells = document.querySelectorAll(".small-grid>div")
        this.restartButton = document.querySelector(".info>button")
        this.score = document.querySelector(".info>p")

        document.addEventListener("keydown", this.onKeyDown)
        document.addEventListener("touchstart", this.initTouch)
        document.addEventListener("touchend", this.endTouch)
        document.addEventListener("touchmove", this.touchMove)
        document.addEventListener("dblclick", (event) => event.preventDefault())
        this.restartButton.addEventListener("click", () => this.restart())
    }

    draw() {
        this.cells.forEach((cell) => cell.removeAttribute("class"))
        this.nextCells.forEach((cell) => cell.removeAttribute("class"))
        this.score.innerHTML = this.tetris.score
        this.drawPlayfield()
        this.drawTetromino()
        this.drawNextTetromino()
        this.drawGhostTetromino()
    }

    initTouch = (event) => {
        this.touchX = event.touches[0].clientX
        this.touchY = event.touches[0].clientY
        this.touchTimeout = setTimeout(() => (this.shortTouch = false), 200)
    }

    endTouch = () => {
        this.touchX = undefined
        clearTimeout(this.touchTimeout)
        if (this.shortTouch) {
            this.endTouchY - this.touchY > SWIPE_THRESHOLD
                ? this.dropDown()
                : this.rotate()
        }
        this.endTouchY = undefined
        this.touchY = undefined
        this.shortTouch = true
    }

    touchMove = (event) => {
        const currentTouchX = event.touches[0].clientX
        const currentTouchY = event.touches[0].clientY

        if (this.touchX - currentTouchX > MOVE_THRESHOLD) {
            this.moveLeft()
            this.touchX = currentTouchX
        }

        if (this.touchX - currentTouchX < -MOVE_THRESHOLD) {
            this.moveRight()
            this.touchX = currentTouchX
        }

        if (this.shortTouch) {
            this.endTouchY = currentTouchY
            return
        }

        if (currentTouchY - this.touchY > MOVE_THRESHOLD) {
            this.moveDown()
            this.touchY = currentTouchY
        }

        this.endTouchY = currentTouchY
    }

    drawPlayfield() {
        for (let row = 0; row < PLAYFIELD_ROWS; row++)
            for (let column = 0; column < PLAYFIELD_COLUMNS; column++) {
                if (!this.tetris.playfield[row][column]) continue
                const name = this.tetris.playfield[row][column]
                const cellIndex = convertPostionToIndex(row, column)

                this.cells[cellIndex].classList.add(name)
            }
    }

    restart() {
        console.log(this.cells)
        this.cells.forEach((cell) => cell.removeAttribute("class"))
        console.log(this.cells)
        this.tetris.init()
        document.addEventListener("keydown", this.onKeyDown)
        document.addEventListener("touchstart", this.initTouch)
        document.addEventListener("touchend", this.endTouch)
        document.addEventListener("touchmove", this.touchMove)
        this.moveDown()
    }

    drawTetromino() {
        const name = this.tetris.tetromino.name
        const tetrominoMatrixSize = this.tetris.tetromino.matrix.length
        for (let row = 0; row < tetrominoMatrixSize; row++)
            for (let column = 0; column < tetrominoMatrixSize; column++) {
                if (!this.tetris.tetromino.matrix[row][column]) continue

                if (this.tetris.tetromino.row + row < 0) continue

                const cellIndex = convertPostionToIndex(
                    this.tetris.tetromino.row + row,
                    this.tetris.tetromino.column + column
                )

                this.cells[cellIndex].classList.add(name)
            }
    }

    drawNextTetromino() {
        const name = this.tetris.nextTetromino.name
        const tetrominoMatrixSize = this.tetris.nextTetromino.matrix.length
        for (let row = 0; row < tetrominoMatrixSize; row++)
            for (let column = 0; column < tetrominoMatrixSize; column++) {
                if (!this.tetris.nextTetromino.matrix[row][column]) continue

                const cellIndex = convertPositionToIndexOfNext(row, 1 + column)

                this.nextCells[cellIndex].classList.add(name)
            }
    }

    drawGhostTetromino() {
        const tetrominoMatrixSize = this.tetris.tetromino.matrix.length
        for (let row = 0; row < tetrominoMatrixSize; row++)
            for (let column = 0; column < tetrominoMatrixSize; column++) {
                if (!this.tetris.tetromino.matrix[row][column]) continue

                const cellIndex = convertPostionToIndex(
                    this.tetris.tetromino.ghostRow + row,
                    this.tetris.tetromino.ghostColumn + column
                )
                if (cellIndex < 0) continue
                this.cells[cellIndex].classList.add("ghost")
            }
    }

    onKeyDown = (event) => {
        switch (event.key) {
            case "ArrowUp":
                this.rotate()
                break
            case "ArrowDown":
                this.moveDown()
                break
            case "ArrowLeft":
                this.moveLeft()
                break
            case "ArrowRight":
                this.moveRight()
                break
            case " ":
                this.dropDown()
                break
            default:
                break
        }
    }

    moveDown() {
        this.tetris.moveTetrominoDown()
        this.draw()
        this.stopLoop()
        this.startLoop()

        if (this.tetris.isGameOver) this.gameOver()
    }

    moveLeft() {
        this.tetris.moveTetrominoLeft()
        this.draw()
    }

    moveRight() {
        this.tetris.moveTetrominoRight()
        this.draw()
    }

    rotate() {
        this.tetris.rotateTetromino()
        this.draw()
    }

    dropDown() {
        this.tetris.dropTetrominoDown()
        this.draw()
        this.stopLoop()
        this.startLoop()

        if (this.tetris.isGameOver) this.gameOver()
    }

    stopLoop() {
        cancelAnimationFrame(this.requestId)
        clearTimeout(this.timeoutId)
    }

    startLoop() {
        this.timeoutId = setTimeout(
            () =>
                (this.requestId = requestAnimationFrame(() => this.moveDown())),
            700
        )
    }

    gameOver() {
        this.stopLoop()
        document.removeEventListener("keydown", this.onKeyDown)
        document.removeEventListener("touchstart", this.initTouch)
        document.removeEventListener("touchend", this.endTouch)
        document.removeEventListener("touchmove", this.touchMove)
        this.gameOverAnimation()
    }

    gameOverAnimation() {
        const filledCells = [...this.cells].filter(
            (cell) => cell.classList.length > 0
        )

        filledCells.forEach((cell, i) => {
            setTimeout(() => cell.classList.add("hide"), i * 10)
            setTimeout(() => cell.removeAttribute("class"), i * 10 + 500)
        })

        setTimeout(() => this.drawSad(), filledCells.length * 10 + 1000)
    }

    drawSad() {
        for (let row = 0; row < SAD.length; row++)
            for (let column = 0; column < SAD[0].length; column++) {
                if (!SAD[row][column]) continue

                const cellIndex = convertPostionToIndex(
                    SAD_TOP_OFFSET + row,
                    column
                )

                this.cells[cellIndex].classList.add("sad")
            }
    }
}
